#!/usr/bin/env python3
'''Swastik Mishra 2017 swastik.mishra@outlook.com

Convert .cliqs format files to .cliq.gpdb which are wwPDB format files
in terms of chemical groups, for starcliques

usage: python ParseBack.py -h [help] -i <cliq-filepath> -g
                        <folder-where-gpdb-is> -c <destination-folder>
'''
import argparse

def parseback(cliqlist):
    '''
    input param = filepath of .cliq file
    global destination_fol
    global gpdbfolder
    '''
    cliqfilelist = destination_fol + 'cliqfilenamelist.txt'
    # write all lines of cliq in cliqs(indices format) to a list
    with (open(cliqlist, 'r')) as allcliqslistfile:
        allcliqslist = allcliqslistfile.readlines()

        for j, cliq in enumerate(allcliqslist):
            allcliqslist[j] = (cliq.strip()).split()
            # Determine gpdbfile from where the clique was obtained
            # change this extension to .pdb if using it on pdb
            gpdbfilename = gpdbfolder + allcliqslist[j][0] + '.gpdb'

            # Read gpdbfile and store all lines as dictionary values
            # The keys will be line numbers
            with open(gpdbfilename, 'r') as gpdbfobj:
                gpdbfile = gpdbfobj.readlines()
                # delete all non-atom records
                gpdbfile = [i for i in gpdbfile if i.startswith("ATOM")]

                # write all groups to a dict
                pdblist = {}

                for k, smgrup in enumerate(gpdbfile):
                    pdblist[k + 1] = gpdbfile[k]
                # print(pdblist)
            gpdbfilename_base = allcliqslist[j][0]

            # for every id in a cliq in the list, find the key in the dict and write
            # to to a new list(for every cliq). Once the whole cliq is done, write the
            # cliqlist to a file for itself.
            groupids = allcliqslist[j][1:]
            cliqinfo = []
            cliqcompo = []
            cliqrescompo = []
            for smid in groupids:
                smid = int(smid) + 1
                # print(smid)
                cliqinfo.append(pdblist[smid])
                cliqcompo.append((pdblist[smid])[12:16].split())
                # Note resno here to later check whether all the cliques
                # are from the same residue or not
                cliqrescompo.append((pdblist[smid])[17:27].strip())
            # print(cliqcompo)
            cliqcompo = [i[0] for i in cliqcompo]
            # cliqrescompo = [i[0] for i in cliqrescompo]

            # if the group-members in the clique are not in the same residue,
            # then only write out the clique
            if not all(x == cliqrescompo[0] for x in cliqrescompo):
                # if int(sys.argv[-1])==1:
                cliqcompo = [cliqcompo[0]]+sorted(cliqcompo[1:])
                # print("cliqcompo is: "+"_".join(cliqcompo))
                # else:
                # cliqcompo.sort()
                newname = str(j + 1) + '_' + gpdbfilename_base + \
                    '_' + '_'.join(cliqcompo) + '.cliq.gpdb'

                newcliqfile = open(destination_fol + newname, 'w')
                # print(newcliqfile)
                for smgroup in cliqinfo:
                    print(smgroup.rstrip(), file=newcliqfile)
                with open(cliqfilelist, 'a') as cliqfilelistapp:
                    cliqfilelistapp.write(destination_fol + newname)
                    cliqfilelistapp.write('\n')

            else:
                print('Star centered at ' + str(j+1) +
                      ' belongs to same residue ')
                # print(cliqrescompo)
                # print(cliqinfo)
    # This is the list of all clique file names that have been stored in destination_fol
    # print(cliqfilelist)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # -i pdbfile -s source_dir -d destination_-b depth
    parser.add_argument("-i", "--input-file", dest='cliqfilename', required=True, help="[REQUIRED]\
                        Name of .cliq file, For example: /home/path-to-dir/1onc.cliq")
    parser.add_argument("-g", "--gpdbdir", default='./', dest='gpdbdir', help="Directory to search\
                        the gpdb in, for any clique/line in .cliq file. Default is current\
                        directory.")
    parser.add_argument("-c", "--cliqdir", dest='cliqdir', default='./', help="Destination\
                        directory, where the .cliq file will be written to.\
                        Deafult value is current directory")
    args = parser.parse_args()
    acliqfile = args.cliqfilename
    gpdbfolder = args.gpdbdir + '/'
    destination_fol = args.cliqdir + '/'

    parseback(acliqfile)
